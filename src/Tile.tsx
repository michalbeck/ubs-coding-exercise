import React, { FC } from 'react'
import { IRecord } from './types'
import { currencyFormatter } from './utils'

interface TileProps extends IRecord {}

const Tile: FC<TileProps> = (props: TileProps) => {
  return (
    <div className='Tile'>
      <h4>{props.name}</h4>
      <div>
        Total spending: {currencyFormatter.format(props.spend)}
      </div>
    </div>
  )
}

export default Tile