import React, { FC, useState } from 'react'
import { IFolder } from './types'
import './styles.css'

interface FolderProps extends IFolder {
  selectedFolder?: string;
  onClick: (props: FolderProps) => void
}

const Folder: FC<FolderProps> = React.memo((props: FolderProps) => {
  const isParent = !!props.children
  const [isOpen, setIsOpen] = useState(false)

  const folderNameClass = props.selectedFolder === props.name ? 'Folder-name--selected' : 'Folder-name'

  return props.name ? (
    <ul className='Folder'>
      <li className='Folder-node'>
        <span className='Folder-toggle' onClick={() => setIsOpen(!isOpen)}>{isOpen ? <>&#x25BE;</> : <>&#x25B8;</>}</span>
        <span className={folderNameClass} onClick={() => props.onClick(props)}>{props.name}</span>
      </li>
      {isParent && isOpen && (
        <li className='Folder-children'>
          {props.children.map((folder: any) => <Folder key={folder.name} {...folder} selectedFolder={props.selectedFolder} onClick={props.onClick} />)}
        </li>
      )}
    </ul>
  ) : null
})

export default Folder