import React, { FC, useEffect, useMemo, useState } from 'react'
import Folder from './Folder'
import Tile from './Tile'
import { currencyFormatter, getFolders } from './utils'
import { IRecord, IFolder } from './types'
import './styles.css'

const App: FC = () => {
  const getMaxSpend = (items: IRecord[]): number => items?.reduce((result, item) => item.spend > result ? item.spend : result, 0)

  const fetchData = async() => {
    const response = await fetch('/data')
    const json = await response.json()
    setData(json)
    setFolders(getFolders(json, ['BCAP1', 'BCAP2', 'BCAP3']))
    setMaxSpend(getMaxSpend(json))
  }

  const [data, setData] = useState<IRecord[]>([])
  const [folders, setFolders] = useState<IFolder[]>([])
  const [selectedFolder, setSelectedFolder] = useState<IFolder>()
  const [maxSpend, setMaxSpend] = useState<number>(0)
  const [filterValue, setFilterValue] = useState<number>(maxSpend)

  useEffect(() => {
    fetchData()
  }, [])

  const handleClick = (folder: IFolder) => {
    setSelectedFolder(folder)
  }
  
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.currentTarget.value
    setFilterValue(parseFloat(value))
  }

  const renderData = useMemo(() => {
    const key = selectedFolder?.bcap as keyof IRecord
    return data.filter(item => item.spend < filterValue && item[key] === selectedFolder?.name)
  }, [data, filterValue, selectedFolder])

  return (
    <div>
      <h1>Pharos Coding Exercise</h1>
      <div className='Application'>
        <div className='Controls'>
          <section className='Navigation'>
            <h3>Navigation</h3>
            {folders?.map((folder: IFolder) => <Folder key={folder.name} {...folder} selectedFolder={selectedFolder?.name} onClick={handleClick} />)}
          </section>
          <section className='Filters'>
            <h3>Filters</h3>
            <div className='Filters-spending'>
              <label>Spending</label>
              <div className='Filters-input'>
                <span>$0</span>
                <input type='range' min='0' max={maxSpend} value={filterValue} onChange={handleChange} />
                <span>{currencyFormatter.format(maxSpend)}</span>
              </div>
            </div>
          </section>
        </div>
        <main className='Main'>
          <h3>Applications</h3>
          <div className='Content'>
            {renderData.map(record => <Tile key={record.id} {...record} />)}
          </div>
        </main>
        
      </div>
    </div>
  )
}

export default App
