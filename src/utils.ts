import { IRecord, IFolder } from './types'

export const currencyFormatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  maximumFractionDigits: 0,
})

export const getFolders = (root: IRecord[], keys: string[]): IFolder[] => {

  if (!keys.length) return []

  const key = keys[0] as keyof IRecord

  const folders = root
    .reduce((result: any[], record: IRecord, index: number) => {
      const name = record[key]
      return result.find(rec => rec.name === name)
        ? result.map(n => n.name === name ? {...n, children: [...n.children, record]} : n)
        : [...result, { name, bcap: key, children: [record] }]
    }, [])
    .sort((a, b) => a.name < b.name ? -1 : 1)

  return folders.map(folder => ({ ...folder, children: getFolders(folder.children, keys.slice(1)) }))
}