export interface IRecord {
  id: string;
  name: string;
  spend: number;
  BCAP1: string;
  BCAP2: string;
  BCAP3: string;
}

export interface IFolder {
  name: string;
  bcap: string;
  children: IFolder[];
}